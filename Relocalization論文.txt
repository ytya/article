○PTAM
・Real-Time SLAM Relocalisation
Brian Williams, Georg Klein and Ian Reid (Oxford)
ICCV2007
https://www.youtube.com/watch?v=uXqDA4do_s0
Sensor: Mono
Method: randomised tree
Features: FAST + BRIEFの前身 
80点のマップで20ms。異なる視点からのリローカライゼーション(数値なし)
マップ中の特徴点全体を探索

・Relocalization Using Virtual Keyframes For Online Environment Map Construction
Sehwan Kim, Christopher Coffin, Tobias Hollerer (California)
VRST2009
Sensor: Mono(Panorama)
Method: Virtual Keyframe
Features: KLT (Keyframe)
3DoF 「Improving the Agility of Keyframe-Based SLAM」

・Fast Relocalization For Visual Odometry Using Binary Features
J. Straub, S. Hilsenbeck, G. Schroth, R. Huitl, A. Moller, E. Steinbach (MIT,TUM)
ICIP2013
https://github.com/jstraub/ptamRosBinaryFeatureRelocalization(PTAM+)
Sensor: Mono
Method: locality sensitive hashing
Features: BRIEF, usBRIEF, ORB (Keyframe)

・Stereo Parallel Tracking and Mapping for robot localization
IROS2015
Taihu Pire; Thomas Fischer; Javier Civera; Pablo De Cristoforis; Julio Jacobo Berlles
Sensor: Stereo
Method: Keyframe
Features: KLT
記述なし（やってない？）

・An image-to-map loop closing method for monocular SLAM
IROS2008
Brian Williams; Mark Cummins; Jose Neira; Paul Newman; Ian Reid; Juan Tardos
Sensor: Mono
Method: Image-to-map
Features: 

・Automatic Relocalization and Loop Closing for Real-Time Monocular SLAM
Pattern Analysis and Machine Intelligence
Brian Williams; Georg Klein; Ian Reid
Sensor: Mono
Method: 
Features: 


○ORB SLAM
・Fast Relocalisation and Loop Closing in Keyframe-Based SLAM
Raul Mur-Artal and Juan D. Tard´os
ICRA 2014
Sensor: Mono
Method: bag of words
Features: ORB


○PoseNet
・PoseNet A Convolutional Network for Real-Time 6-DOF Camera Relocalization
Cambridge
ICCV2015
Sensor: 
Method: CNN
Features: 


○RGB-D
・Exploiting Uncertainty in Regression Forests for accurate camera relocalization
Julien Valentin, Matthias NieBner, Jamie Shotton, Andrew Fitzgibbon, Shahram Izadi, Philip Torr (Oxford, MS)
CVPR2015
Sensor: 
Method: Regression Forests
Features: 

・Real-Time RGB-D Camera Relocalization via Randomized Ferns for Keyframe Encoding
Ben Glocker, Jamie Shotton, Antonio Criminisi, and Shahram Izadi (Microsoft)
TVCG2015
Sensor: RGB-D
Method: model-based ICP
Features: None
Keyframeをコード化してBlockHDによりキーフレーム間の類似度を元にした距離を定義。
Keyframeとして登録するframeのサンプリングもこの距離を元に決定。
類似するk個のKeyframeを検索して、kNN+WAPでICPかけて、エラー最小のものを選択。
グラフたくさん。
オフラインのORBに比べれば低いけど、リアルタイムでもかなりいいよ。
単に縮小した画像でKeyframe検索するよりコード化した方がよい(58→66%)。
k個のKeyframeの重み付け平均よりもkNNのICPの方がよかった。
RGB-Dフルで使った方がよい。
7シーンのデータセット(屋内小規模)で成功率66%。

・6D Relocalisation for RGBD Cameras Using Synthetic View Regression
Andrew P. Gee, Walterio Mayol-Cuevas (Bristol)
BMVC2012
Sensor: RGB-D
Method: model-based ICP
Features: None


○Keyframe-Based
・Improving the Agility of Keyframe-Based SLAM
Georg Klein and David Murray
ECCV2008
keyframe元論文



○ Fast and accurate relocalization for keyframe-based SLAM using geometric model selection
VR2016
富士通


○Modelling uncertainty in deep learning for camera relocalization
ICRA2016
Sensor: RGB-D





